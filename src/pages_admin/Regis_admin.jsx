import react, {useEffect, useState, Fragment} from 'react';
import { Button, FormGroup, FormControl, Image } from "react-bootstrap";
import { Link, Redirect } from 'react-router-dom';
import '../components/style.css';
import login from '../components/bg_login.png';

function RegisAdmin() {
    const[name, setName] = useState('');
    const[email, setEmail] = useState('');
    const[password, setPassword] = useState('');
    const[password2, setPassword2] = useState('');
    const[redirect, setRedirect] = useState(false);
    const[flag, setFlag] = useState(0);

    const onChangeName = (e) => {
        const value = e.target.value;
        setName(value);
    }

    const onChangeEmail = (e) => {
        const value = e.target.value;
        setEmail(value);
    }

    const onChangePassword2 = (e) => {
      const value = e.target.value;
      setPassword2(value);
    }

    const checkPassword = (e) => {
      if (password2!==password ) {
        setFlag(2);
      }else {
        setFlag(0);
      }
    }

    const onChangePassword = (e) => {
        const value = e.target.value;
        setPassword(value);
    }

    const DaftarAkun = () => {
        const data = {
            name: name,
            email: email,
            password: password
        }
        
        fetch('http://kelompok4.dtstakelompok1.com/api/v1/admin/add',{
            method:'post',
            headers: {     
                'content-type':'application/json',
            },
            mode: 'cors',
            body: JSON.stringify(data),
        }).then(function(res){
            console.log(res);
            console.log(data);
            if(res != null){
                setRedirect(true);
                setFlag(1);
            } else {
                setRedirect(false);
                setFlag(2);
            }
        })
    }

    return(
        <Fragment>
            {
                redirect && (
                    <Redirect to="/successAdmin" />
                )
            }
            <div>
                <div class="content-bg bg-admin">
                    <img class="image-bg" src={login}/>
                </div>

                <div className="register-form">
                <div className="col-lg-5">
                    <div className="card rounded-lg">
                    <div className="Login">
                        <div className="col-md-12">
                        <h3 className="text-center">Daftar Akun Admin</h3>
                        </div>
                        <div className="col-md-12">
                            <div className="form-group">
                            <label for="exampleInputName1">Nama</label>
                            <input type="text" className="form-control" name="name"  placeholder="Nama Lengkap" value={name} onChange={onChangeName} />
                            </div>
                            <div className="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" className="form-control" name="email" aria-describedby="emailHelp" placeholder="Email" value={email} onChange={onChangeEmail} />
                            </div>
                            <div className="form-group">
                            <label for="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" name="password" placeholder="Password" value={password} onChange={onChangePassword} />
                            </div>
                            <div className="form-group">
                            <label for="exampleInputConfirmPassword">Konfirmasi Password</label>
                            <input type="password" className="form-control" name="password2" placeholder="Konfirmasi Password" value={password2} onChange={onChangePassword2} onKeyUp={checkPassword} />
                            </div>
                            {flag === 2 && 
                            <div>
                                <label for="cek password" style={{paddingLeft: "100px", color: "red"}}>Password tidak sesuai !</label>
                            </div>
                            }
                            
                            <button type="submit" className="btn btn-primary btn-block" onClick={DaftarAkun}>Daftar</button>
                            <div class="text-center">
                                <h5 className="text-center">Sudah memiliki akun? Kembali ke<a href="/LoginAdmin"> Login Admin</a></h5>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                    
                
            </div>
        </Fragment>
    )    
}

export default RegisAdmin